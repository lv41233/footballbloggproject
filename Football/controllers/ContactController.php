<?php

$path = $_SERVER['DOCUMENT_ROOT'];
$path .= "/mobile-shop-demo/core/Database.php";
include_once($path);

class ContactController
{
    protected $db;

    public function __construct()
    {
        $this->db = new Database;
    }

    public function all()
    {
        $query = $this->db->pdo->query('SELECT * FROM contactus');

        return $query->fetchAll();
    }

    public function store($request)
    {
        $query = $this->db->pdo->prepare('INSERT INTO contactus (namesurname, email, date, message,reference) VALUES (:namesurname, :email, :date, :message,:reference)');
        $query->bindParam(':namesurname', $request['namesurname']);
        $query->bindParam(':email', $request['email']);
        $query->bindParam(':date', $request['date']);
        $query->bindParam(':message', $request['message']);
		$query->bindParam(':reference', $request['reference']);
        $query->execute();

        return header('Location: contact.php');
    }

    public function edit($id)
    {
        $query = $this->db->pdo->prepare('SELECT * FROM contact WHERE id = :id');
        $query->execute(['id' => $id]);

        return $query->fetch();
    }

    public function update($id, $request)
    {

        $query = $this->db->pdo->prepare('UPDATE contactus SET namesurname = :namesurname, email = :email, date = :date, message = :message, reference=:reference WHERE id= :id');
        $query->execute([
            'namesurname' => $request['namesurname'],
            'email' => $request['email'],
            'date' => $request['date'],
            'message' => $request['message'],
			'reference'=>$request['reference'],
            'id' => $id
        ]);

        return header('Location: contact.php');
    }

    public function destroy($id)
    {
        $query = $this->db->pdo->prepare('DELETE FROM contactus WHERE id = :id');
        $query->execute(['id' => $id]);

        return header('Location: contact.php');
    }
}