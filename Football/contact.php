<!DOCTYPE html>
<html>
<head>
	<title>Conctact Form</title>
	<link rel="stylesheet" type="text/css" href="contactstyle.css">
</head>
<body>
	<div class="container">
		<div class="header">
			<div class="logo">
				<img src="logo.png" >
				<p>SPORT BLOG</p>
			</div>
		</div>
		
		<div class="topnav" id="myTopnav">
				<a href="index.php">HOME</a>
				<a href="#">LAJME</a>
				<a href="historiku.php">HISTORIKU</a>
				<a href="#">BLOG</a>
				<a href="contact.php" target="_blank" >KONTAKTI</a>
				<a href="register.php">REGJISTROHU</a>
				<a href="login.php">KYCU</a>
				<a href="javascript:void(0);" class="icon" onclick="myFunction()">
					<i class="fa fa-bars"></i>
				</a>
			</div>
	
	<div class="main">
		<h1>CONTACT FORM</h1>
		<form name="forma" >
				<label for="emri">Your Name: </label> <br> 
				<input type="text" id="emri" name="emri" class="text" placeholder="Name" required>
				<span class="error emri">
					
				</span>
				<br>
				<br>
				<br>
				<label for="email">Your Email:   </label> <br>
				<input type="email"  class="text" name="email" class="email" placeholder="Email" required>
				<span class="error email">
                     </span>
                    <br>
					<br>
					<br>
					<label for="message">Message :  </label> <br> <br> 
                    <textarea name="message" id="message" cols="50" rows="5" placeholder="Enter your message here"></textarea>
                    <span class="error message">
                     </span>
                    
                    <br>
			
					<button type="button" id="sent">Login</button>
			<br>
		</form>
	</div>
	
	<div class="footer">
			<div class="credits">&copy UNTITLED. ALL RIGHTS RESERVED. DESIGNED BY SPORTY TEAM</div>
		
			<ul class="social-icons">
			  <li><a href="#"><img class="social-icon" src="http://sporty.wp4life.com/images/social-icons/facebook.png" alt="Facebook"> </a></li>
			  <li><a href="#"><img class="social-icon" src="http://sporty.wp4life.com/images/social-icons/twitter.png" alt="Twitter"></a></li>
			  <li><a href="#"><img class="social-icon" src="http://sporty.wp4life.com/images/social-icons/flickr.png" alt="Flickr"></a></li>
			  <li><a href="#"><img class="social-icon" src="http://sporty.wp4life.com/images/social-icons/linkedin.png" alt="Linked in"></a></li>
			  <li><a href="#"><img class="social-icon" src="http://sporty.wp4life.com/images/social-icons/google+.png" alt="Google+"></a></li>
			  <li><a href="#"><img class="social-icon" src="http://sporty.wp4life.com/images/social-icons/vimeo.png" alt="Vimeo"></a></li>
			  <li><a href="#"><img class="social-icon" src="http://sporty.wp4life.com/images/social-icons/youtube.png" alt="YouTube"></a></li>
			</ul>
		</div>

	</div>
	<script>
			function myFunction() {
				var x = document.getElementById("myTopnav");
				if (x.className === "topnav") {
					x.className += " responsive";
				} else {
					x.className = "topnav";
				}
			}
		</script>
	<script src="contactvalidate.js" type="text/javascript" lang="javascript"></script>

</body>
</html>