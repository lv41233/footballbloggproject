<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Dream Team</title>
    <link rel="stylesheet" href="style.css">
    <link rel="shortcut icon" href="favicon.ico" />
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick-theme.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
   
</head>
<body>
<div class="container">
          <!-- Header -->
          
          <div class = "topnav">
                <a>
                <?php if(isset($_SESSION['name'])): ?>
            <a href="#">WELCOME, <?php echo $_SESSION['name']; ?></a>
            <li><a class = "logout" href="logout.php">Logout</a></li>
            
           <?php endif; ?>
          </div>
          <div class="header">
              <div class="logo">
                  <img src="logo.png" >
                  
                  <p>SPORT BLOG</p>
              </div>
          </div>
          
          <!-- Navigation Bar -->
          <div class="topnav" id="myTopnav">
            <a href="index.php">HOME</a>
            <a href="#">LAJME</a>
            <a href="historiku.php">HISTORIKU</a>
            <a href="#">BLOG</a>
            <a href="contact.php" target="_blank" >KONTAKTI</a>
            <a href="register.php">REGJISTROHU</a>
            <a href="login.php">KYCU</a>
            <a href="javascript:void(0);" class="icon" onclick="myFunction()">
              <i class="fa fa-bars"></i>
            </a>
          </div>
          <div class="slider">
            <div><img src="gashi.png"></div>
              <div><img src="stadio.png" alt=""></div>
              <div><img src="qatarstadium.png" alt=""></div>
              <div><img src="teams.png" alt=""></div>
              <div><img src="match.png" alt=""></div>
          </div>