var btn = document.getElementById("sent");
btn.addEventListener("click", validateForm);

function validateForm() {
    let result = true;
    var emri = document.forma.emri.value;

    let errorEmri = document.querySelector(".emri");
    if (emri.trim().length == 0) {
        alert("Please fill in the 'Name' box.");
        document.forma.emri.focus();
        errorEmri.innerHTML = "Plotesojeni fushen!"
        errorEmri.className = "error emri active";
        return result = false;
    }else{
    	errorEmri.innerHTML = "";
    	errorEmri.className = "error emri";
    }

    let errorMbiemri = document.querySelector(".mbiemri");
    var mbiemri = document.forma.mbiemri.value;
    if (mbiemri.trim().length == 0) {
        alert("Please fill in the 'Last Name' box.");
        document.forma.mbiemri.focus();
        errorMbiemri.innerHTML = "Plotesojeni fushen!"
        errorMbiemri.className = "error mbiemri active";
        return result = false;
    }else{
    	errorMbiemri.innerHTML = "";
    	errorMbiemri.className = "error mbiemri";
    }

    let errorUsername = document.querySelector(".username");
    var username = document.forma.username.value;
    if (username.trim().length == 0) {
        alert("Please fill in the 'UserName' box.");
        document.forma.username.focus();
        errorUsername.innerHTML = "Plotesojeni fushen!"
        errorUsername.className = "error username active";
        return result = false;
    }else{
    	errorUsername.innerHTML = "";
    	errorUsername.className = "error username";
    }

    let errorEmail = document.querySelector(".email");
    var email = document.forma.email.value;
    if (email.trim().length == 0) {
        alert("Please fill in the 'Email' box.");
        document.forma.email.focus();
        errorEmail.innerHTML = "Plotesojeni fushen!"
        errorEmail.className = "error email active";
        return result = false;
    }else{
    	errorEmail.innerHTML = "";
    	errorEmail.className = "error email";
    }

     let errorPassword = document.querySelector(".password");
     var password = document.forma.password.value;
     if (password.trim().length == 0) {
         alert("Please fill in the 'Password' box.");
         document.forma.password.focus();
         errorPassword.innerHTML = "Plotesojeni fushen!"
         errorPassword.className = "error password active";
         return result = false;
     }else{
     	errorPassword.innerHTML = "";
     	errorPassword.className = "error password";
    }
    

    let errorGjinia = document.querySelector(".gjinia");
    if(document.forma.gjinia[0].checked == false && document.forma.gjinia[0].checked == false){
        alert("Please choose your Gender: Male or Female");
        errorGjinia.innerHTML = "Zgjedh gjinine!"
        errorGjinia.className = "error gjinia active";
        return result = false;
    }else{
    	errorGjinia.innerHTML = "";
    	errorGjinia.className = "error gjinia";
    }

    let errorOpsion = document.querySelector(".opsion");
    if(document.forma.opsion.selectedIndex == 0){
        alert("Please select your Age.");
        document.forma.opsion.focus();
        errorOpsion.innerHTML = "Zgjedh moshen"
        errorOpsion.className = "error opsion active";
        return result = false;
    }else{
    	errorOpsion.innerHTML = "";
    	errorOpsion.className = "error opsion";
    }
    let errorTiku = document.querySelector(".tiku");
    if(document.forma.tiku.checked == false){
        alert("Please tell us if you are admin or not.");
        document.forma.tiku.focus();
        errorTiku.innerHTML = "Gabim"
        errorTiku.className = "error tiku active";
        
    }
    else{
        errorTiku.innerHTML = "";
    	errorTiku.className = "error tiku";
    }
    return result;
};