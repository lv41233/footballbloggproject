<?php
    require './controllers/UserController.php';

    $user = new UserController;
    $users = $user->all();
?>

<?php
include('includes/header.php');
?>
<body>
    
<?php
include('includes/contain.php');
?>
        
          <?php
include('includes/footer.php');
?>
  </div>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"> </script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.js"></script>
  <script>          
       $(document).ready(function(){
          $('.slider').slick({
              dots: true,
              infinite: true,
              fade: true,
              adaptiveHeight: true,
              autoplay: true,
              autoplaySpeed: 5000,
              cssEase: 'linear'    
          });
      });
  </script>
  <script>
    function myFunction() {
      var x = document.getElementById("myTopnav");
      if (x.className === "topnav") {
        x.className += " responsive";
      } else {
        x.className = "topnav";
      }
    }
  </script>
</body>
</html>