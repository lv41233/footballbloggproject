<?php
  require '../controllers/UserController.php';
  include '../core/Database.php';
   $user = new UserController;
   $users = $user->all();
?>
<!DOCTYPE html>
<html>
<head>
  <title>Mobile Store - Admin Page</title>
  <link rel="stylesheet" type="text/css" href="../adminstyle.css">
</head>
<body>
  <div class="wrapper">

  <nav>

    <header>
      <span></span>
      <?php echo $_SESSION['name']; ?>
    </header>

    <!-- SIDEBAR -->
    <?php include '../includes/sidebar.php'; ?>

  </nav>

  <div class="content">
    <div class="content-title">
       <h1 class="page-title">Dashboard</h1>
    </div>
    <div class="main-content">
      <table class="table">
         <tr class="table-head">
          <th>ID</th>
          <th>Name</th>
          <th>Email</th>
          <th>Admin</th>
          <th>Date of Creation</th>
          <th></th>
          <th></th>
        </tr>
        <?php foreach($users as $user): ?>
          <tr>
            <td><?php echo $user['id']; ?></td>
            <td><?php echo $user['name']; ?></td>
            <td><?php echo $user['email']; ?></td>
            <td><?php echo $user['is_admin']; ?></td>
            <td><?php echo $user['created_at']; ?></td>
            <td><a href="delete-user.php?id=<?php echo $user['id']?>">Delete</a></td>
            <td><a href="edit-user.php?id=<?php echo $user['id']?>">Edit</a></td>
          </tr>
        <?php endforeach; ?>
      </table>
        
    </div>
  </div>

  </div>
</body>
</html>
