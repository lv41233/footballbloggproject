<?php
    require '../controllers/AboutUsController.php';
    include '../core/Database.php';
    $about = new AboutUsController;

    if(isset($_GET['id'])) {
        $aboutId = $_GET['id'];
    }

    $currentAbout = $about->edit($aboutId);

    if(isset($_POST['submitted'])) {
        $about->update($aboutId, $_POST);
    }
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Edit AboutUs</title>
    <link rel="stylesheet" type="text/css" href="../css/adminstyle.css">
  
  <style>
    body {font-family: Arial, Helvetica, sans-serif;}

    #contact-form-group {
      padding-top: 1%;
    }
    .contact-phone {
      font-family: 'Open Sans', sans-serif;
      font-size: 18px;
      color: #292929;
      margin-right: 5%;
      line-height: 30px;
      font-weight: 300;
      padding-left: 1%;
    }
    #heading-contact-phone{
        margin-right: 5%;
    }
    #title-contact-phone{
        margin-right: 7%;
    }
    #description-contact-phone{
        margin-right: 3.5%;
    }
    #inputButtons {
      width: 50%;
      box-sizing: border-box;
      padding: 20px;
      margin-bottom: 25px;
      border: 2px solid #e9eaea;
      color: #3e3e40;
      font-size: 14px;
      outline: none;
      transition: all 0.5s ease;
    }
    .addButton{
      background-color: #4CAF50; /* Green */
      border: none;
      color: white;
      margin-left: 30%;
      padding: 16px 32px;
      text-align: center;
      text-decoration: none;
      font-size: 16px;
      cursor: pointer;
    }
    .cancelButton{
      background-color: red; 
      border: none;
      color: white;
      margin-left: 5%;
      padding: 16px 32px;
      text-align: center;
      text-decoration: none;
      font-size: 16px;
      cursor: pointer;
      text-decoration: none;
    }
  </style>
</head>
<body>
    <form action="" method="POST">
       <div id="contact-form-group" class="form-group">
            <label id="heading-contact-phone" class="contact-phone">Welcome:</label>
            <input id="inputButtons" type="text" value="<?php echo $currentAbout['welcome']; ?>" name="welcome" style="position:relative;left:5px;">
        </div>
        <div class="form-group">
            <label id="title-contact-phone" class="contact-phone">MVV:</label>
            <input id="inputButtons" type="text" value="<?php echo $currentAbout['mvv']; ?>" name="mvv" style="position:relative;left:19px;">
        </div>
        <div class="form-group">
            <label id="title-contact-phone" class="contact-phone">Misioni:</label>
            <input id="inputButtons" type="text" value="<?php echo $currentAbout['misioni']; ?>" name="misioni" style="position:relative;left:-3px;">
        </div>
        <div class="form-group">
            <label id="title-contact-phone" class="contact-phone">Vizioni:</label>
            <input id="inputButtons" type="text" value="<?php echo $currentAbout['vizioni']; ?>" name="vizioni" style="position:relative;left:5px;">
        </div>
		<div class="form-group">
            <label id="title-contact-phone" class="contact-phone">Vlerat:</label>
            <input id="inputButtons" type="text" value="<?php echo $currentAbout['vlerat']; ?>" name="vlerat" style="position:relative;left:12px;">
        </div>
        <button type="submit" name="submitted" class="addButton">Update</button>
        <button class="cancelButton">Close</button>
    </form>
</body>
</html>
