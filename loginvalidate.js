var btn = document.getElementById("sent");
btn.addEventListener("click", validateForm);

function validateForm() {
    let result = true;
    var username = document.forma.username.value;
    let errorUsername = document.querySelector(".username");
    
    if (username.trim().length == 0) {
        alert("Please fill in the 'UserName' box.");
        document.forma.username.focus();
        errorUsername.innerHTML = "Plotesojeni fushen!"
        errorUsername.className = "error username active";
        return result = false;
    }else{
    	errorUsername.innerHTML = "";
    	errorUsername.className = "error username";
    }

    let errorPassword = document.querySelector(".password");
    var password = document.forma.password.value;
    if (password.trim().length == 0) {
        alert("Please fill in the 'Password' box.");
        document.forma.password.focus();
        errorPassword.innerHTML = "Plotesojeni fushen!"
        errorPassword.className = "error password active";
        return result = false;
    }else{
        errorPassword.innerHTML = "";
        errorPassword.className = "error password";
   } 
   return result;
}
