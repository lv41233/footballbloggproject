<?php
    require './controllers/UserController.php';
  

    $user = new UserController;

    if(isset($_POST['submitted'])) {
        $user->store($_POST);
    }
?>


<!DOCTYPE html>

<head>
    <title>SIGN UP</title>
    <link rel="stylesheet" href="registerstyle.css">
    <link rel="shortcut icon" href="favicon.ico" />
</head>

<body>
    <div class="contain">
           <!-- Header -->
           <div class="header">
                <div class="logo">
                    <img src="logo.png" >
                    <p>SPORTBLOG</p>
                </div>
            </div>
            
            <!-- Navigation Bar -->
            <div class="topnav" id="myTopnav">
                    <a href="index.php">HOME</a>
                    <a href="#">LAJME</a>
                    <a href="historiku.php">HISTORIKU</a>
                    <a href="#">BLOG</a>
                    <a href="contact.php" target="_blank" >KONTAKTI</a>
                    <a href="register.php">REGJISTROHU</a>
                    <a href="login.php">KYCU</a>
                    <a href="javascript:void(0);" class="icon" onclick="myFunction()">
                        <i class="fa fa-bars"></i>
                    </a>
                </div>
        <div class="container">
            <h1>SIGN UP</h1>
            <form name="forma" method="POST" >
				<label for="emri">Your Name: </label>
				<input type="text" id="emri" name="fullName" class="text" placeholder="Name" required>
				<span class="error emri">
					
				</span>
				<br>
				<!-- <label for="mbiemri">Last Name: </label>
				<input type="text" id="mbiemri" name="lastname" class="text" placeholder="LastName" required>
				<span class="error mbiemri">
					
                </span>
                
                <br>
                <label for="username">Username:</label>
				<input type="text" id="username" name="username" class="text" placeholder="Username"  required>
				<span class="error username">
                 </span>
                 -->
                    <br>

                    <label for="email">Your Email:   </label> 
                    <input type="email"  class="text" name="email" class="email" placeholder="Email" required>
                    <span class="error email">
                     </span>
                    
                    <br>
                 
                    <label for="password">Password: </label>
                    <input type="password" name="password" placeholder="Password"  class="text">
                     <span class="error password">
                     </span>
                    
                    <br>
				<!-- <label for="gjinia">Your Gender: </label>
				<input type="radio" name="gjinia" id="male" value="Male"> Male
				<input type="radio" name="gjinia" id="female" value="Female"> Female
				<span class="error gjinia">
					
				</span>
				<br>
				<label for="mosha">Your Age:</label>
				<select id="mosha" name="opsion">
					<option value="0">-- Select --</option>
					<option value="0-18 years">0-18 years</option>
                    <option value="18-30 years">18-30 years</option>
                    <option value="30-45 years">30-45 years</option>
                    <option value="45-60 years">45-60 years</option>
                    <option value="60+ years">60+ years</option>
				</select>
				<span class="error opsion">
					
				</span> -->
				<br>
				<label for="tiku">is Admin? </label>
				<input type="checkbox" id="tiku" name="is_admin" > Yes
                <span class="error tiku "></span>
                <br>
                
				<button type="submit" id="sent" name="submitted">Register</button>
			</form>
        </div>
        
    
        <div class="footer">
              <div class="credits">&copy UNTITLED. ALL RIGHTS RESERVED. DESIGNED BY SPORTY TEAM</div>
          
              <ul class="social-icons">
                <li><a href="#"><img class="social-icon" src="http://sporty.wp4life.com/images/social-icons/facebook.png" alt="Facebook"> </a></li>
                <li><a href="#"><img class="social-icon" src="http://sporty.wp4life.com/images/social-icons/twitter.png" alt="Twitter"></a></li>
                <li><a href="#"><img class="social-icon" src="http://sporty.wp4life.com/images/social-icons/flickr.png" alt="Flickr"></a></li>
                <li><a href="#"><img class="social-icon" src="http://sporty.wp4life.com/images/social-icons/linkedin.png" alt="Linked in"></a></li>
                <li><a href="#"><img class="social-icon" src="http://sporty.wp4life.com/images/social-icons/google+.png" alt="Google+"></a></li>
                <li><a href="#"><img class="social-icon" src="http://sporty.wp4life.com/images/social-icons/vimeo.png" alt="Vimeo"></a></li>
                <li><a href="#"><img class="social-icon" src="http://sporty.wp4life.com/images/social-icons/youtube.png" alt="YouTube"></a></li>
              </ul>
        </div>
          
  
    </div>
    
    <script src="registervalidate.js" type="text/javascript" lang="javascript"></script>
    <script>
            function myFunction() {
              var x = document.getElementById("myTopnav");
              if (x.className === "topnav") {
                x.className += " responsive";
              } else {
                x.className = "topnav";
              }
            }
          </script>


</body>