<html>
    <head>
        <title>Historiku</title>
        <link rel="stylesheet" href="historikustyle.css">
    </head>
    <body>
        <div class="contain">
                    <!-- Header -->
                    <div class="header">
                         <div class="logo">
                             <img src="logo.png" >
                             <p>SPORT BLOG</p>
                         </div>
                     </div>
                     
                     <!-- Navigation Bar -->
                     <div class="topnav" id="myTopnav">
                            <a href="index.php">HOME</a>
                            <a href="#">LAJME</a>
                            <a href="historiku.php">HISTORIKU</a>
                            <a href="#">BLOG</a>
                            <a href="contact.php" target="_blank" >KONTAKTI</a>
                            <a href="register.php">REGJISTROHU</a>
                            <a href="login.php">KYCU</a>
                            <a href="javascript:void(0);" class="icon" onclick="myFunction()">
                                <i class="fa fa-bars"></i>
                            </a>
                        </div>
         <div class="container">
                
            <div class="title"><h1>Historiku i shkurter i futbollit te Kosoves</h1></div>
            <br>
            <br>
            <h2>PERIUDHA E PARE</h2>
            <br>
            <p>
                <img src="http://ffk-kosova.com/wp-content/uploads/2018/12/historikuperiudh0.jpg"><br>
                <br>
                Topi i parë në Kosovë është sjellur nga një student i Grenoblit më 1919, por thuhet se më 1914 futboll kanë luajtur Austro-Hungarezët që kanë shërbyer në Kosovë në atë kohë.<br>    
                Futbolli në Kosovë është luajtur shumë herët, por klubet e para janë formuar në vitin 1922 në Gjakovë dhe KF “Prishtina” në Prishtinë, e më pastaj formohen edhe klubet e tjera.<br>                
                Garat nuk kanë qenë të rregullta për arsye të situatës jostabile që ka mbretëruar në atë kohë. 
            </p>
            <strong><h2>PERIUDHA E DYTE</h2></strong>
            <p>
                <img src="http://ffk-kosova.com/wp-content/uploads/2018/12/historikuperiudha3-768x431.jpg"><br>
                <br>
                Kjo periudhë fillon pas luftës së dytë Botërore prej vitit 1945 e deri më 1991. Në këtë kohë futbolli në Jugosllavi përparon në mënyrë të shpejtë e bashkë me të edhe futbolli kosovar, kur formohet edhe Federata e Futbollit të Kosovës më 1948, si degë e Federatës së Futbollit të Jugosllavisë. Në Ligën e parë jugosllave dallohen më së shumti KF “Prishtina”, kurse një vit anëtar i kësaj lige ishte edhe KF “Trepça” nga Mitrovica. Liga e dytë shtetërore vazhdimisht kishte nga 3-4 klube kosovare. Në këto dy liga Federative garonin Sllovenia, Kroacia, Bosnja e Hercegovina, Serbia, Mali i Zi,Vojvodina, Kosova dhe Maqedonia, të gjitha në një ligë unike.<br>               
                Kosova e kishte ligën e vet të parë, ku skuadra kampione kalonte direkt në Ligën e dytë jugosllave, pastaj kishte Ligën e dytë dhe të tretë.<br>                
                Kosova në këtë kohë kishte reprezentues, referë federativë dhe punëtorë të shumtë të nivelit më të lartë.<br>
            </p>
            <p>
                <strong>Kampionët e Ligës së Kosovës në këtë periudhë kanë qenë:</strong>
                <br>
            </p>
            <p>1945 Jedinstvo (Prishtinë)</p>
            <p>1947 Trepça (Mitrovicë)</p>
            <p>1947-48 Proleteri (Prishtinë)</p>
            <p>1948-49 Trepça</p>
            <p>1950 Trepça</p>
            <p>1951 Kosova (Prishtinë)</p>
            <p>1952 Trepça</p>
            <p>1953-54 Kosova</p>
            <p>1954-55 Trepça</p>
            <p>1955-56 Rudari (Stantërg)</p>
            <p>1956-57 Rudniku (Hajvali)</p>
            <p>1957-58 Rudari</p>
            <p>1958-59 Prishtina</p>
            <p>1959-60 Rudari</p>
            <p>1960-61 Prishtina</p>
            <p>1961-62 Buduqnosti (Pejë)</p>
            <p>1962-63 C.Zvezda (Gjilan)</p>
            <p>1963-64 Slloga (Lipjan)</p>
            <p>1964-65 Slloga</p>
            <p>1965-66 Buduqnosti</p>
            <p>1966-67 Obiliqi (Kastriot)</p>
            <p>1967-68 Vëllaznimi(Gjakovë)</p>
            <p>1968-69 Vëllaznimi</p>
            <p>1969-70 Vëllaznimi</p>
            <p>1970-71 Vëllaznimi</p>
            <p>1971-72 Obiliqi</p>
            <p>1972-73 Fushë-Kosova</p>
            <p>1973-74 Vëllaznimi</p>
            <p>1974-75 Liria (Prizren)</p>
            <p>1975-76 KXEK Kosova (Kastriot)</p>
            <p>1976-77 Prishtina</p>
            <p>1977-78 Buduqnosti</p>
            <p>1978-79 Prishtina</p>
            <p>1979-80 Vëllaznimi</p>
            <p>1980-81 Liria</p>
            <p>1981-82 Vëllaznimi</p>
            <p>1982-83 KNI ”Ramiz Sadiku” (Prishtinë)</p>
            <p>1983-84 Liria</p>
            <p>1984-85 C.Zvezda</p>
            <p>1985-86 Vëllaznimi</p>
            <p>1986-87 Liria</p>
            <p>1987-88 C.Zvezda</p>
            <p>1988-89 Buduqnosti</p>
            <p>1989-90 Vëllaznimi</p>
            <p>1990-91 Fushë-Kosova</p>
            
            <p>
            <strong>Plotesim:</strong>
            Në këtë etapë, për shkak të situatave politike në Kosovë, përkatësisht për shkak të demostratave gjithëpopullore të shqiptarëve më 1981 dhe më 1989, kampionatet e Ligës së Kosovës 1980-81 dhe 1988-89, me vendimet e pushtetit të atëhershëm, janë ndërprerë që në fillim të stinorit pranveror dhe me dekret, kampion janë shpallur skuadrat që në ato momente kanë qenë në pozitat e para të renditjes tabelare (Liria, përkatësisht Buduqnosti).
            <br>
            </p>
            <p><strong>Shënim:</strong> Duhet theksuar se në këtë periudhë skuadrat më të suksesshme të Kosovës kanë qenë të përfshira në kategoritë më të larta të shtetit të atëhershëm (Jugosllavisë), prandaj Liga e Kosovës ka qenë për nga cilësia më e dobët.</p>
            <br>
            <h2><strong>PERIUDHA E TRETË</strong></h2><br>
            <p> <img src="http://ffk-kosova.com/wp-content/uploads/2018/12/historikuperiudha2.jpg"> <br>
                Në kohën e shpërbërjes së Jugosllavisë, pikërisht në vitin 1991 futbolli kosovar ishte në zenitin e saj dhe kishte vazhdimisht 4-5 klube në të dy ligat federative. Por, përkeqësimi i situatës politike reflektohej edhe në sport. Ishte gati e pamundur të luhej më futboll, e sidomos jo në Kosovë. Kjo bëri që në gusht të vitit 1991, klubi më i mirë i Kosovës, Prishtina me një traditë të ndritshme në Ligën e parë jugosllave të braktisë Ligën profesioniste të Jugosllavisë, sepse nuk kishte më garancë për sigurinë as të lojtarëve, as të zyrtarëve të tjerë, por as të simpatizuesve kosovar.<br>
                Reaksioni i pushtetit të Millosheviqit ishte pa presedan në historinë e sportit. Ai suprimoi të gjitha klubet e të gjitha sporteve dhe definitivisht i largoi nga fushat e veta të gjithë sportistët e Kosovës, siç bëri edhe me punëtorët që i la në rrugë. Të gjitha nivelet e ligave të futbollit në Kosovë u zhdukën për një ditë nga dora kriminele e pushtetit serb.<br>
                Në vitin 1991, pas këtij akti të pashembullt të shtetit serbo-jugosllav, futbolldashësit kosovar treguan qartë se nuk mund të jetojnë pa futboll dhe në kushte absurde për shekullin e 20-të, formuan FEDERATËN E FUTBOLLIT TË KOSOVËS, të pavarur nga Jugosllavia me të gjitha organet e saja: kryetarin, bordin prej 12 anëtarëve nga 7 qendrat rajonale, komisionet, shoqatat etj..<br>
                Ndeshja e parë u zhvillua në stadiumin e Flamurtarit në Prishtinë, më 13 shtator 1991, që shënoi fillimin e kampionatit të parë të pavarur të Kosovës. Liga e parë e Kosovës numëronte 20 skuadra, ndërsa ligat tjera mvarësisht nga Regjionet, por ndeshjet luheshin vetëm nëpër fusha të improvizuara, në kushte shumë të vështira.<br>
                Nëpër stadiumet e futbollit të Kosovës luanin tani 8% e popullatës së gjithmbarshme të saj, pra vetëm serbët. Maltretimet, burgosjet, rrahjet ishin të pranishme në çdo javë, por futbolli në Kosovë mbijetoi dhe u luajtë deri në janar të vitit 1998, kur filloi fushata luftarake serbe ndaj popullatës civile të Kosovës. Edhe gjatë kësaj periudhe (1991-1998)në Kosovë luhej futbolli sipas rregullave të UEFA-s dhe FIFA-s, dhe çdo ndryshim i rregullave të tyre menjëherë aplikohej në sistemin e garave tona.<br>
            </p>
            <p><strong>Kampionët e Kosovës në periudhën e tretë kanë qenë:</strong></p>
            <p>
                1991-92 Prishtina <br>
                1992-93 Trepça <br>
                1993-94 Dukagjini (Klinë) <br>
                1994-95 Liria<br>
                1995-96 Prishtina<br>
                1996-97 Prishtina<br>
                1997-98 Kampionati, për shkak të luftës, ndërpritet pas përfundimit të stinorit vjeshtor<br>
                1998-99 Për shkak të luftës, kampionati nuk u zhvillua fare.
            </p>
            <h2><strong>PERIUDHA E KATËRT</strong></h2>
            <p><img src="http://ffk-kosova.com/wp-content/uploads/2018/12/historikuperiudha1-768x510.jpg"><br>
                Faza e IV-të e Futbollit në Kosovë filloi menjëherë pas përfundimit të luftës në Kosovë më 1999, kur u riorganizua FEDERATA E FUTBOLLIT E KOSOVËS. Konstituohet Kuvendi, miratohet Statuti, konfirmohen klubet pjesëmarrëse, caktohen komisionet, shoqatat, etj.<br>
                Liga e parë numëronte 18 skuadra, Liga e dytë – dy grupe me nga 14 suadra.<br>
                Prej vitit 1991 vazhdimisht në futbollin e Kosovës është luajtur sipas rregullave të UEFA-së dhe FIFA-s, derisa interesimi për futboll ishte i madh, përkundër faktit se edhe më tutje FFK-ja vazhdonte të ishte jashtë organizmave ndërkombëtare. Megjithë insistimin e vazhdueshëm për t’u bërë pjesë e UEFA-s e FIFA-s një gjë e tillë ishte pamundur të realizohej për shkak të rrethanave politike.<br>
                Kushtet infrastrukturale prej vitit 1999 e deri në anëtarësimin e Kosovës në UEFA e FIFA kanë qenë të vështira dhe futbolli ka mbijetuar vetëm falë sponsorëve vullnetarë, të cilët me vite të tëra kanë dhënë mjetet e tyre personale.
            </p>
            <p><strong>Kampionët e Kosovës pas çlirimit</strong></p>
            <p>
                    1999-2000 Prishtina <br>
                    2000-01 Prishtina<br>
                    2001-02 Besiana<br>
                    2002-03 Drita (Gjilan)<br>
                    2003-04 Prishtina<br>
                    2004-05 Besa (Pejë)<br>
                    2005-06 Besa<br>
                    2006-07 Besa<br>
                    2007-08 Prishtina<br>
                    2008-09 Prishtina<br>
                    2009-10 Trepça<br>
                    2010-11 Hysi<br>
                    2011-12 Prishtina<br>
                    2012-13 Prishtina<br>
                    2013-14 Llamkos Kosova<br>
                    2014-15 Feronikeli<br>
            </p>
            <h2><strong>PERIUDHA E PESTË</strong></h2>
            <p>
                <img src="http://ffk-kosova.com/wp-content/uploads/2018/12/vokrri-1.png"><br>
                Pas shumë peripecive, sakrificave të shumë gjeneratave, më në fund Federata e Futbollit e Kosovës, nën drejtimin e Fadil Vokrrit, pas shumë takimeve dhe lobimeve, arriti që t’i thyejë të gjitha barrierat dhe në maj të vitit 2016 të bëhet fillimisht anëtare e UEFA-s, e më pas edhe e FIFA-s, duke u bërë kësisoj pjesë e familjes së madhe të futbollit evropian dhe atij botëror. Kjo periudhë vlerësohet edhe si më e rëndësishmja për zhvillimin e mëtejmë të futbollit kosovar, pasi tani përveç se Kosova merr pjesë në kualifikimet e Evropianit dhe Botërorit me kombëtaren e saj, edhe klubet po paraqiten në garat evropiane. Madje, Kombëtarja A në formatin e ri garues të UEFA-s, Ligën e Kombeve, që nisi në vitin 2018, arriti që të triumfojë në Ligën D, duke u promovuar në Ligën C për edicionin e radhës, si dhe ta sigurojë pjesëmarrjen në “play-off” për Euro 2020, që pa dyshim paraqet suksesin më të madh të Kombëtares sonë. E me paraqitje të suksesshme po paraqiten edhe kombëtaret e grupmoshave tjera, si dhe ato në konkurrencën e femrave dhe në futsall.<br>
            </p>
            <p><strong>Kampionët pas pranimit në UEFA</strong></p>
            <p>
                    2015-16 Feronikeli<br>
                    2016-17 Trepça’89<br>
                    2017-18 Drita<br>
            </p>
            <p><strong>Fituesit e Kupës së Kosovës që nga mëvetësimi</strong></p>
            <p>
                    1991/92: Trepça (pa lojë)<br>
                    1992/93: Flamurtari (në finale e mundi Trepçën 1:0)<br>
                    1993/94: Prishtina (në finale e mundi Vëllaznimin 2:1)<br>
                    1994/95: Prishtina (në finale e mundi Dukagjinin 1:0)<br>
                    1995/96: Flamurtari (në finale e mundi Dukagjinin 2:1)<br>
                    1996/97: 2 Korriku (pa lojë, Gjilani nuk u paraqit)<br>
                    1999/00: Gjilani (në finale e mundi Besianën 1:0)<br>
                    2000/01: Drita (në finale e mundi Gjilanin 1:1 – 6:5 pen)<br>
                    2001/02: Besiana (në finale e mundi Gjilanin 2:1)<br>
                    2002/03: KEK-u (në finale e mundi Prishtinën 3:1)<br>
                    2003/04: Kosova Pr.(në finale e mundi Besën 1:0)<br>
                    2004/05: Besa (në finale e mundi KEK-un 3:2)<br>
                    2005/06: Prishtina (në finale e mundi Drenicën 1:1 – 5:4 pen)<br>
                    2006/07: Liria (në finale e mundi Flamurtarin 0:0 – 3:0 pen)<br>
                    2007/08: Vëllaznimi (në finale e mundi Trepçën’89 2:0<br>
                    2008/09: Hysi (në finale e mundi Prishtinën 2:1)<br>
                    2009/10: Liria (në finale e mundi Vëllaznimin 2:1)<br>
                    2010/11: Besa (në finale e mundi Prishtinën 2:1)<br>
                    2011/12: Trepça ’89 (në finale e mundi Ferizajn 3:0)<br>
                    2012/13: Prishtina (në finale e mundi Ferizajn 1:1 – 4:3 pen)<br>
                    2013/14: Feronikeli (në finale e mundi Hajvalinë 2:1)<br>
                    2014/15: Feronikeli (në finale e mundi Trepçën’89 1:1 – 5:4 pen)<br>
                    2015/16: Prishtina (në finale e mundi Dritën 2:1)<br>
                    2016/17 Besa (në finale e mundi Llapin 1:1 – 4:2 pen)<br>
                    2017/18 Prishtina (në finale e mundi Vëllaznimin 1:1 – 5:4 pen)<br>
            </p>

        </div>
    
        <div class="footer">
                <div class="credits">&copy UNTITLED. ALL RIGHTS RESERVED. DESIGNED BY SPORTY TEAM</div>
            
                <ul class="social-icons">
                <li><a href="#"><img class="social-icon" src="http://sporty.wp4life.com/images/social-icons/facebook.png" alt="Facebook"> </a></li>
                <li><a href="#"><img class="social-icon" src="http://sporty.wp4life.com/images/social-icons/twitter.png" alt="Twitter"></a></li>
                <li><a href="#"><img class="social-icon" src="http://sporty.wp4life.com/images/social-icons/flickr.png" alt="Flickr"></a></li>
                <li><a href="#"><img class="social-icon" src="http://sporty.wp4life.com/images/social-icons/linkedin.png" alt="Linked in"></a></li>
                <li><a href="#"><img class="social-icon" src="http://sporty.wp4life.com/images/social-icons/google+.png" alt="Google+"></a></li>
                <li><a href="#"><img class="social-icon" src="http://sporty.wp4life.com/images/social-icons/vimeo.png" alt="Vimeo"></a></li>
                <li><a href="#"><img class="social-icon" src="http://sporty.wp4life.com/images/social-icons/youtube.png" alt="YouTube"></a></li>
                </ul>
            
        </div>
    </div>
    <script>
            function myFunction() {
              var x = document.getElementById("myTopnav");
              if (x.className === "topnav") {
                x.className += " responsive";
              } else {
                x.className = "topnav";
              }
            }
          </script>
</body>
</html>